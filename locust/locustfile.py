from locust import HttpUser, task
import warnings

warnings.filterwarnings("ignore")

class GetEventUserUser(HttpUser):

    def on_start(self):
        self.client.verify = False

    @task
    def get_events_bell_center(self):
        self.client.get("/events-gateway/bell/api/v2/events")
        self.client.get("/events-gateway/bell/api/v2/events/1")