package com.altrem.jug.demo.events.gateway;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_ORIGINAL_REQUEST_URL_ATTR;
import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR;
import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

@Component
public class LoggingGatewayFilter
    implements GlobalFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingGatewayFilter.class);

    @Override
    public Mono<Void> filter(
        final ServerWebExchange exchange,
        final GatewayFilterChain chain) {

        final String originalUri =
            Optional.ofNullable(exchange.getAttribute(GATEWAY_ORIGINAL_REQUEST_URL_ATTR)).map(uris -> (Set<URI>) uris)
                .filter(uris -> !uris.isEmpty()).map(uris -> uris.iterator().next().toString()).orElse("(Unknown)");

        final Route route = exchange.getAttribute(GATEWAY_ROUTE_ATTR);
        final URI routeUri = exchange.getAttribute(GATEWAY_REQUEST_URL_ATTR);
        final String correlationId =
            exchange.getRequest().getHeaders().getOrDefault("X-Correlation-Id", List.of("(None)")).get(0);

        LOGGER.info("Inbound [%s] routed towards [Routing ID: %s, URI: %s] [CorrelationId: %s]".formatted(originalUri,
            route.getId(), routeUri, correlationId));

        return chain.filter(exchange);
    }
}
