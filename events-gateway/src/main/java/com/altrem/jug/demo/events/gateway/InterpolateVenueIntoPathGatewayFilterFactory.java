package com.altrem.jug.demo.events.gateway;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

@Component
public class InterpolateVenueIntoPathGatewayFilterFactory
    extends AbstractGatewayFilterFactory<InterpolateVenueIntoPathGatewayFilterFactory.Config> {

    public static class Config {
    }

    public InterpolateVenueIntoPathGatewayFilterFactory() {

        super(Config.class);
    }

    @Override
    public GatewayFilter apply(
        final InterpolateVenueIntoPathGatewayFilterFactory.Config config) {

        return this::applyFilter;
    }

    private Mono<Void> applyFilter(
        final ServerWebExchange exchange,
        final GatewayFilterChain chain) {

        final Map<String, String> uriTemplateVariables = ServerWebExchangeUtils.getUriTemplateVariables(exchange);
        final String venueCode = uriTemplateVariables.getOrDefault("venueCode", null);
        final Route route = exchange.getAttribute(GATEWAY_ROUTE_ATTR);

        final URI newUri = renderNewUri(route, venueCode);
        exchange.getAttributes().put(GATEWAY_ROUTE_ATTR,
            Route.async().asyncPredicate(route.getPredicate()).id(route.getId()).order(route.getOrder()).uri(newUri)
                .metadata(route.getMetadata()).replaceFilters(route.getFilters()).build());

        final ServerHttpRequest.Builder builder = exchange.getRequest().mutate();
        chain.filter(exchange.mutate().request(builder.build()).build());

        return chain.filter(exchange);
    }

    private URI renderNewUri(
        final Route route,
        final String venueCode) {

        try {
            return new URI(route.getUri().toString().replace("venueCode", venueCode));
        } catch (final URISyntaxException e) {
            e.printStackTrace();
        }

        return URI.create("/");
    }
}
