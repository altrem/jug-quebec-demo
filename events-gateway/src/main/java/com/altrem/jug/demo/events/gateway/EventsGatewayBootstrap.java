package com.altrem.jug.demo.events.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventsGatewayBootstrap {

    public static void main(
        final String[] args) {

        SpringApplication.run(EventsGatewayBootstrap.class, args);
    }
}
