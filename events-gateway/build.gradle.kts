import org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
import org.gradle.api.tasks.testing.logging.TestLogEvent.FAILED

plugins {
    id("org.springframework.boot")
    id("io.spring.dependency-management")
}

group = rootProject.group
version = rootProject.version

extra["springCloudVersion"] = "2021.0.1"

java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot", "spring-boot-starter-webflux")
    implementation("org.springframework.boot", "spring-boot-starter-actuator")
    implementation("org.springframework.boot", "spring-boot-actuator-autoconfigure")

    implementation("org.springframework.cloud", "spring-cloud-starter-gateway")
    implementation("org.springframework.cloud", "spring-cloud-starter-kubernetes-client-config")

    implementation("io.micrometer", "micrometer-core")
    implementation("io.micrometer", "micrometer-registry-prometheus")

    testImplementation("org.springframework.boot", "spring-boot-starter-test")
}

dependencyManagement {
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.test {
    environment("LOG_APPENDER_NAME", "Console")
    testLogging {
        events(FAILED)
        exceptionFormat = FULL
        showExceptions = true
        showCauses = true
        showStackTraces = true
    }
}

springBoot {
    buildInfo()
}

tasks.getByName<org.springframework.boot.gradle.tasks.bundling.BootBuildImage>("bootBuildImage") {
    environment = mapOf(
        "BPE_APPLICATION_VERSION" to "${project.version}",
        "BPE_APPLICATION_NAME" to "events.gateway",
    )
    imageName = "altrem/events-gateway:${project.version}"
}