
import org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
import org.gradle.api.tasks.testing.logging.TestLogEvent.FAILED

plugins {
    java
    id("io.spring.dependency-management")
}

group = "com.gitlab.altrem.jug"
version = project.findProperty("dynamicVersion") ?: "0.0.0-SNAPSHOT"

subprojects {
    apply(plugin = "java")
    apply(plugin = "io.spring.dependency-management")


    repositories {
        mavenCentral()
    }

    dependencies {
        implementation("org.springframework.boot", "spring-boot-starter")
        implementation("org.apache.commons", "commons-lang3", "3.8.1")
        implementation("org.apache.commons", "commons-text", "1.9")
    }

    dependencyManagement {
        val springBootVersion: String by project
        imports {
            mavenBom("org.springframework.boot:spring-boot-dependencies:$springBootVersion")
        }
    }

    tasks.test {
        useJUnitPlatform()
    }

    configure<JavaPluginExtension> {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
}

tasks.test {
    testLogging {
        events(FAILED)
        exceptionFormat = FULL
        showExceptions = true
        showCauses = true
        showStackTraces = true
    }
}