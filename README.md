# JUG Quebec - Spring Webflux Demo

## Grafana

The default login/password of Grafana with Kube Prometheus Stack is `admin/prom-opeator`. A simple Spring Boot dashboard can be found [here](https://grafana.com/grafana/dashboards/6756).

## Reloader

`skataker/reloader` is the tool we use in our Kubernetes cluster to automatically watch for changes on an object. Here, deploying a new ConfigMap for our gateway will trigger
a rollout of the gateway pods.

## Hosts File

The following entries must be added to your `/etc/hosts` file.

```shell
192.168.2.53 local.cluster.dev
192.168.2.53 prometheus.local.cluster.dev
```

The IP address `192.168.2.53` can be replaced by the `External-IP` of your Traefik `Service` object.

## PostgreSQL Helm Chart Dependency

- If your first configuration fails to authenticate (bad password, wrong Helm parameter used, etc), you must manually delete the `PersistentVolumeClaim` before reinstalling the chart, otherwise the postgres pod will remain bound to the original credentials.

## Spring Cloud Gateway & Spring Cloud Kubernetes

- Your custom filters, including global filters such as the one for logging, need to have correct names to be discovered
  by Spring Cloud Gateway. They need to be suffixed with `GatewayFilterFactory` or `GatewayFilter` in the case of global
  filters.
- For Spring Cloud Kubernetes to work, you absolutely need to have the `k8s/events-gateway/roles.yaml` Kubernetes
  objects installed on your cluster, since the framework needs permissions to read a `PropertySource` from a ConfigMap.