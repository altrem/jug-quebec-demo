#!/bin/bash

# Gateway and its configuration
kubectl apply -f k8s/events-gateway-config/configmap.yaml
kubectl apply -f k8s/events-gateway/roles.yaml
kubectl apply -f k8s/events-gateway/deployment.yaml
kubectl apply -f k8s/events-gateway/service.yaml
kubectl apply -f k8s/events-gateway/ingress.yaml

# Events Service for Bell Center and Videotron Center
helm repo add bitnami https://charts.bitnami.com/bitnami
helm dependency build k8s/events-service
helm upgrade --install --atomic events-service-bell-sandbox k8s/events-service
helm upgrade --install --atomic events-service-videotron-sandbox k8s/events-service
helm upgrade --install --atomic --set image.repository=altrem/events-legacy-service events-service-colisee-sandbox k8s/events-service