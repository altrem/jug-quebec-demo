#!/bin/bash

mkcert -install local.cluster.dev
kubectl create secret tls local-cluster-pem --cert local.cluster.dev.pem --key local.cluster.dev-key.pem # Make sure to select the proper K8S context!
rm local.cluster.dev.pem
rm local.cluster.dev-key.pem

mkcert -install prometheus.local.cluster.dev
kubectl create secret tls prm-local-cluster-pem --cert prometheus.local.cluster.dev.pem --key prometheus.local.cluster.dev-key.pem # Make sure to select the proper K8S context!
rm prometheus.local.cluster.dev.pem
rm prometheus.local.cluster.dev-key.pem

mkcert -install grafana.local.cluster.dev
kubectl create secret tls grafana-local-cluster-pem --cert grafana.local.cluster.dev.pem --key grafana.local.cluster.dev-key.pem # Make sure to select the proper K8S context!
rm grafana.local.cluster.dev.pem
rm grafana.local.cluster.dev-key.pem