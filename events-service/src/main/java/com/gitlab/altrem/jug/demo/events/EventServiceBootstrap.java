package com.gitlab.altrem.jug.demo.events;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@SpringBootApplication
@EnableR2dbcRepositories
@ComponentScan(basePackages = { "com.gitlab.altrem.jug.demo" })
public class EventServiceBootstrap {

    public static void main(
        final String[] args) {

        SpringApplication.run(EventServiceBootstrap.class, args);
    }
}
