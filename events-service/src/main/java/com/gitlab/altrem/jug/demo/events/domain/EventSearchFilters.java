package com.gitlab.altrem.jug.demo.events.domain;

import java.util.Optional;

public class EventSearchFilters {

    private String organizationCode;

    private EventSearchFilters() {

    }

    public static EventSearchFilters empty() {

        return new EventSearchFilters();
    }

    public EventSearchFilters onOrganizationCode(
        final String organizationCode) {

        this.organizationCode = organizationCode;
        return this;
    }

    public Optional<String> getOrganizationCode() {

        return Optional.ofNullable(organizationCode);
    }
}
