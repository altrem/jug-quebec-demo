package com.gitlab.altrem.jug.demo.events.web.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.reactivestreams.Subscription;
import org.slf4j.MDC;

import reactor.core.CoreSubscriber;
import reactor.util.context.Context;

public class InterthreadMdcLifter<T>
    implements CoreSubscriber<T> {

    private final CoreSubscriber<T> coreSubscriber;

    private final Context context;

    public InterthreadMdcLifter(
        final Context context,
        final CoreSubscriber<T> coreSubscriber) {

        this.context = context;
        this.coreSubscriber = coreSubscriber;
    }

    @Override
    public void onSubscribe(
        final Subscription subscription) {

        copyToMdc(currentContext());
        coreSubscriber.onSubscribe(subscription);
    }

    @Override
    public void onNext(
        final T obj) {

        copyToMdc(currentContext());
        coreSubscriber.onNext(obj);
    }

    @Override
    public void onError(
        final Throwable t) {

        copyToMdc(currentContext());
        coreSubscriber.onError(t);
    }

    @Override
    public void onComplete() {

        copyToMdc(currentContext());
        coreSubscriber.onComplete();
    }

    @Override
    public Context currentContext() {

        return context;
    }

    private void copyToMdc(
        final Context context) {

        if (!context.isEmpty()) {
            final Map<String, String> map = computeMapFromContext(context);
            final Map<String, String> copyOfContextMap =
                Optional.ofNullable(MDC.getCopyOfContextMap()).orElseGet(HashMap::new);
            copyOfContextMap.putAll(map);
            MDC.setContextMap(copyOfContextMap);
        } else {
            MDC.clear();
        }
    }

    private Map<String, String> computeMapFromContext(
        final Context context) {

        return context.stream().filter(contextEntry -> contextEntry.getKey().equals("X-Correlation-Id"))
            .collect(Collectors.toMap(contextEntry -> contextEntry.getKey().toString(),
                contextEntry -> contextEntry.getValue().toString()));
    }
}
