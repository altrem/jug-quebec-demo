package com.gitlab.altrem.jug.demo.events.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Component;

import com.gitlab.altrem.jug.demo.events.domain.Event;
import com.gitlab.altrem.jug.demo.events.domain.EventId;
import com.gitlab.altrem.jug.demo.events.domain.EventRepository;
import com.gitlab.altrem.jug.demo.events.domain.EventSearchFilters;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class SqlEventRepository
    implements EventRepository {

    private static final String SEARCH_ALL =
        "SELECT * FROM EVENTS e LEFT JOIN ORGANIZATIONS o ON e.EVENT_ORGANIZATION_ID = o.ORGANIZATION_ID";

    private static final String SEARCH_BY_ORGANIZATION =
        "SELECT * FROM EVENTS e LEFT JOIN ORGANIZATIONS o ON e.EVENT_ORGANIZATION_ID = o.ORGANIZATION_ID WHERE o.ORGANIZATION_CODE = :organizationCode";

    private static final String FIND_BY_ID =
        "SELECT * FROM EVENTS e "
            + "LEFT JOIN ORGANIZATIONS o ON o.ORGANIZATION_ID = e.EVENT_ORGANIZATION_ID "
            + "LEFT JOIN EVENT_ARTISTS ea ON ea.EVENT_ID = ea.EVENT_ID "
            + "LEFT JOIN ARTISTS a ON ea.ARTIST_ID = a.ARTIST_ID "
            + "WHERE e.EVENT_ID = :eventId";

    private final DatabaseClient databaseClient;

    private final EventRowMapper eventRowMapper;

    @Autowired
    public SqlEventRepository(
        final DatabaseClient databaseClient,
        final EventRowMapper eventRowMapper) {

        this.databaseClient = databaseClient;
        this.eventRowMapper = eventRowMapper;
    }

    @Override
    public Flux<Event> findAllByCriteria(
        final EventSearchFilters searchFilters) {

        final DatabaseClient.GenericExecuteSpec executeSpec =
            searchFilters.getOrganizationCode().map(String::toUpperCase)
                .map(orgCode -> databaseClient.sql(SEARCH_BY_ORGANIZATION).bind("organizationCode", orgCode))
                .orElseGet(() -> databaseClient.sql(SEARCH_ALL));

        return executeSpec.map(eventRowMapper::mapSummary).all();
    }

    @Override
    public Mono<Event> findById(
        final EventId eventId) {

        return databaseClient.sql(FIND_BY_ID).bind("eventId", eventId.getValue()).map(eventRowMapper::mapLink).all()
            .collectList().mapNotNull(eventRowMapper::map);
    }
}
