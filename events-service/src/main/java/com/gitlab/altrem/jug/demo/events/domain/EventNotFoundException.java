package com.gitlab.altrem.jug.demo.events.domain;

public class EventNotFoundException
    extends RuntimeException {

    private static final String ERROR_CODE = "EVENT_NOT_FOUND";

    public EventNotFoundException(
        final EventId eventId) {

        super("Event with ID [%s] could not be found.".formatted(eventId.getValue()));
    }

    public String getErrorCode() {

        return ERROR_CODE;
    }
}
