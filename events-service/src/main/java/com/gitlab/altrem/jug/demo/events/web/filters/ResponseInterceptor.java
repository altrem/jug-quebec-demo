package com.gitlab.altrem.jug.demo.events.web.filters;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.Logger;
import reactor.util.Loggers;

public class ResponseInterceptor
    extends ServerHttpResponseDecorator {

    private static final Logger LOGGER = Loggers.getLogger(ResponseInterceptor.class);

    private String responseBody;

    public ResponseInterceptor(
        final ServerHttpResponse delegate) {

        super(delegate);
    }

    @Override
    public Mono<Void> writeWith(
        final Publisher<? extends DataBuffer> body) {

        final Flux<DataBuffer> buffer = Flux.from(body);

        return super.writeWith(buffer.doOnNext(dataBuffer -> {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            try {
                Channels.newChannel(outputStream).write(dataBuffer.asByteBuffer().asReadOnlyBuffer());
                responseBody =
                    outputStream.toString(Optional.ofNullable(getHeaders().getContentType()).map(MediaType::getCharset)
                        .orElse(StandardCharsets.UTF_8));

            } catch (final IOException exception) {
                LOGGER.error("Error while writing the DataBuffer: ", exception);
            } finally {
                closeStream(outputStream);
            }
        }));
    }

    private void closeStream(
        final ByteArrayOutputStream outputStream) {

        try {
            outputStream.close();
        } catch (final IOException e) {
            LOGGER.error("Error while closing the data stream:", e);
        }
    }

    public String getResponseBody() {

        return responseBody;
    }
}
