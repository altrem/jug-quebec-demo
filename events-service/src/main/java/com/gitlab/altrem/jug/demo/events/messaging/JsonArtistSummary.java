package com.gitlab.altrem.jug.demo.events.messaging;

public class JsonArtistSummary {

    private Integer id;

    private String code;

    private String name;

    public Integer getId() {

        return id;
    }

    public void setId(
        final Integer id) {

        this.id = id;
    }

    public String getCode() {

        return code;
    }

    public void setCode(
        final String code) {

        this.code = code;
    }

    public void setName(
        final String name) {

        this.name = name;
    }

    public String getName() {

        return name;
    }
}
