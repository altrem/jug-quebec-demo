package com.gitlab.altrem.jug.demo.events.persistence;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.gitlab.altrem.jug.demo.events.domain.Event;
import com.gitlab.altrem.jug.demo.events.domain.EventId;
import com.gitlab.altrem.jug.demo.events.domain.artists.Artist;
import com.gitlab.altrem.jug.demo.events.domain.artists.ArtistId;

import io.r2dbc.spi.Row;

@Component
public class EventRowMapper {

    public Event mapSummary(
        final Row row) {

        return new Event(
            EventId.from(row.get("EVENT_ID", Integer.class)),
            row.get("EVENT_NAME", String.class),
            row.get("ORGANIZATION_CODE", String.class),
            List.of());
    }

    public EventArtistLink mapLink(
        final Row row) {

        final EventArtistLink eventArtistLink = new EventArtistLink();
        eventArtistLink.setEventId(row.get("EVENT_ID", Integer.class));
        eventArtistLink.setEventOrganizationCode(row.get("ORGANIZATION_CODE", String.class));
        eventArtistLink.setEventName(row.get("EVENT_NAME", String.class));
        eventArtistLink.setArtistId(row.get("ARTIST_ID", Integer.class));
        eventArtistLink.setArtistCode(row.get("ARTIST_CODE", String.class));
        eventArtistLink.setArtistName(row.get("ARTIST_NAME", String.class));

        return eventArtistLink;
    }

    public Event map(
        final List<EventArtistLink> links) {

        if (links.isEmpty()) {
            return null;
        }

        final List<Artist> artists =
            links.stream().filter(l -> l.getArtistId() != null)
                .map(l -> new Artist(ArtistId.from(l.getArtistId()), l.getArtistCode(), l.getArtistName()))
                .collect(Collectors.toList());

        final EventArtistLink eventArtistLink = links.get(0);
        return new Event(
            EventId.from(eventArtistLink.getEventId()),
            eventArtistLink.getEventName(),
            eventArtistLink.getEventOrganizationCode(),
            artists);
    }
}
