package com.gitlab.altrem.jug.demo.events.web.filters;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.jboss.logging.MDC;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;

import reactor.core.publisher.Flux;
import reactor.util.Logger;
import reactor.util.Loggers;

public class RequestInterceptor
    extends ServerHttpRequestDecorator {

    private static final Logger LOGGER = Loggers.getLogger(RequestInterceptor.class);

    public RequestInterceptor(
        final ServerHttpRequest delegate) {

        super(delegate);
    }

    @Override
    public Flux<DataBuffer> getBody() {

        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        return super.getBody().doOnNext(dataBuffer -> {
            try {

                Channels.newChannel(outputStream).write(dataBuffer.asByteBuffer().asReadOnlyBuffer());
                final String requestBody =
                    outputStream.toString(Optional.ofNullable(getHeaders().getContentType()).map(MediaType::getCharset)
                        .orElse(StandardCharsets.UTF_8));

                LOGGER.info("Inbound - [%s %s] - [Headers: %s] - [MDC: %s] - Body: %s".formatted(
                    getDelegate().getMethodValue(), getDelegate().getPath().value(),
                    getDelegate().getHeaders().toSingleValueMap(), MDC.getMap(), requestBody));
            } catch (final IOException exception) {
                LOGGER.error("Error while writing the DataBuffer to stream: ", exception);
                LOGGER.info("Outbound - [%s %s] - [Headers: %s] - [MDC: %s] - Body: %s".formatted(
                    getDelegate().getMethodValue(), getDelegate().getPath().value(),
                    getDelegate().getHeaders().toSingleValueMap(), MDC.getMap(),
                    "(Couldn't be decoded from output stream)"));
            } finally {
                closeStream(outputStream);
            }
        });
    }

    private void closeStream(
        final ByteArrayOutputStream outputStream) {

        try {
            outputStream.close();
        } catch (final IOException exception) {
            LOGGER.error("Error while closing the stream: ", exception);
        }
    }
}
