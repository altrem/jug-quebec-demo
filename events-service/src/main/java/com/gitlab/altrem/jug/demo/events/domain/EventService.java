package com.gitlab.altrem.jug.demo.events.domain;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class EventService {

    private final EventRepository eventRepository;

    @Autowired
    public EventService(
        final EventRepository eventRepository) {

        this.eventRepository = eventRepository;
    }

    public Flux<Event> getEvents(
        final String organizationCode) {

        final EventSearchFilters searchFilters = EventSearchFilters.empty();
        Optional.ofNullable(organizationCode).filter(StringUtils::isNotEmpty)
            .ifPresent(searchFilters::onOrganizationCode);

        return eventRepository.findAllByCriteria(searchFilters);
    }

    public Mono<Event> getEventById(
        final EventId eventId) {

        return eventRepository.findById(eventId).switchIfEmpty(Mono.error(new EventNotFoundException(eventId)));
    }
}
