package com.gitlab.altrem.jug.demo.events.web.config;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.context.annotation.Configuration;

import reactor.core.publisher.Hooks;
import reactor.core.publisher.Operators;

@Configuration
public class InterthreadMdcConfiguration {

    private final String MDC_CONTEXT_REACTOR_KEY = InterthreadMdcConfiguration.class.getName();

    @PostConstruct
    private void contextOperatorHook() {

        Hooks.onLastOperator(MDC_CONTEXT_REACTOR_KEY, Operators.lift((
            scannable,
            coreSubscriber) -> new InterthreadMdcLifter<>(coreSubscriber.currentContext(), coreSubscriber)));

        Hooks.onEachOperator(MDC_CONTEXT_REACTOR_KEY, Operators.lift((
            scannable,
            coreSubscriber) -> new InterthreadMdcLifter<>(coreSubscriber.currentContext(), coreSubscriber)));
    }

    @PreDestroy
    private void cleanupHook() {

        Hooks.resetOnLastOperator(MDC_CONTEXT_REACTOR_KEY);
    }
}
