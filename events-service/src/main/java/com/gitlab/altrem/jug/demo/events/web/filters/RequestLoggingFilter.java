package com.gitlab.altrem.jug.demo.events.web.filters;

import org.apache.commons.lang3.StringUtils;
import org.jboss.logging.MDC;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import reactor.core.publisher.Mono;
import reactor.util.Logger;
import reactor.util.Loggers;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 11)
public class RequestLoggingFilter
    implements WebFilter {

    private static final Logger LOGGER = Loggers.getLogger(RequestLoggingFilter.class);

    @Override
    public Mono<Void> filter(
        final ServerWebExchange exchange,
        final WebFilterChain chain) {

        final ServerHttpRequest request = exchange.getRequest();
        final ServerHttpResponse response = exchange.getResponse();

        if (requestHasNoBody(request)) {
            logRequestWithoutBody(request);
        }

        final ServerWebExchange interceptedExchange =
            exchange.mutate().request(new RequestInterceptor(request)).response(new ResponseInterceptor(response))
                .build();

        return chain.filter(interceptedExchange).doOnSuccess(x -> logResponse(interceptedExchange))
            .doOnError(throwable -> logError(throwable, interceptedExchange));
    }

    private boolean requestHasNoBody(
        final ServerHttpRequest request) {

        return request.getHeaders().getContentType() == null || request.getHeaders().getContentLength() <= 0;
    }

    private void logRequestWithoutBody(
        final ServerHttpRequest request) {

        LOGGER.info("Inbound - [%s %s] - [Headers: %s] - [MDC: %s] - Body: (None)".formatted(request.getMethodValue(),
            request.getPath().value(), request.getHeaders().toSingleValueMap(), MDC.getMap()));
    }

    private void logError(
        final Throwable throwable,
        final ServerWebExchange exchange) {

        final var request = exchange.getRequest();

        LOGGER.error("Outbound - [%s %s] - [Headers: %s] - [MDC: %s] - An exception has occurred. %s".formatted(
            request.getMethodValue(), request.getPath().value(), request.getHeaders().toSingleValueMap(), MDC.getMap(),
            throwable));
    }

    private void logResponse(
        final ServerWebExchange exchange) {

        final var response = exchange.getResponse();
        final var request = exchange.getRequest();

        final String payload = response instanceof ResponseInterceptor r ? r.getResponseBody() : StringUtils.EMPTY;

        LOGGER.info("Outbound/%d - [%s %s] - [Headers: %s] - [MDC: %s] - Body: %s".formatted(
            response.getRawStatusCode(), request.getMethodValue(), request.getPath().value(),
            request.getHeaders().toSingleValueMap(), MDC.getMap(), payload));
    }
}
