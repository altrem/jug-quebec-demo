package com.gitlab.altrem.jug.demo.events.web;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.gitlab.altrem.jug.demo.events.domain.Event;
import com.gitlab.altrem.jug.demo.events.domain.artists.Artist;
import com.gitlab.altrem.jug.demo.events.messaging.JsonArtistSummary;
import com.gitlab.altrem.jug.demo.events.messaging.JsonEvent;
import com.gitlab.altrem.jug.demo.events.messaging.JsonEventSummary;

@Component
public class JsonEventMapper {

    public JsonEventSummary mapSummary(
        final Event event) {

        final JsonEventSummary jsonEventSummary = new JsonEventSummary();
        jsonEventSummary.setId(event.getId().getValue());
        jsonEventSummary.setName(event.getName());
        jsonEventSummary.setOrganizationCode(event.getOrganizationCode());

        return jsonEventSummary;
    }

    public JsonEvent map(
        final Event event) {

        final JsonEvent jsonEvent = new JsonEvent();
        jsonEvent.setId(event.getId().getValue());
        jsonEvent.setName(event.getName());
        jsonEvent.setEventOrganizationCode(event.getOrganizationCode());

        final Set<JsonArtistSummary> jsonAirports =
            event.getAttendingArtists().stream().map(this::mapAirport).collect(Collectors.toSet());

        jsonEvent.setAttendingArtists(jsonAirports);

        return jsonEvent;
    }

    private JsonArtistSummary mapAirport(
        final Artist artist) {

        final JsonArtistSummary jsonArtistSummary = new JsonArtistSummary();
        jsonArtistSummary.setId(artist.getId().getValue());
        jsonArtistSummary.setCode(artist.getCode());
        jsonArtistSummary.setName(artist.getName());

        return jsonArtistSummary;
    }
}
