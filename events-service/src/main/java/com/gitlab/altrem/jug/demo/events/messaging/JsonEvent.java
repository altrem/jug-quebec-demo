package com.gitlab.altrem.jug.demo.events.messaging;

import java.util.Set;

public class JsonEvent {

    private Integer id;

    private String name;

    private String eventOrganizationCode;

    private Set<JsonArtistSummary> attendingArtists;

    public Integer getId() {

        return id;
    }

    public void setId(
        final Integer id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(
        final String name) {

        this.name = name;
    }

    public String getEventOrganizationCode() {

        return eventOrganizationCode;
    }

    public void setEventOrganizationCode(
        final String eventOrganizationCode) {

        this.eventOrganizationCode = eventOrganizationCode;
    }

    public Set<JsonArtistSummary> getAttendingArtists() {

        return attendingArtists;
    }

    public void setAttendingArtists(
        final Set<JsonArtistSummary> attendingArtists) {

        this.attendingArtists = attendingArtists;
    }
}
