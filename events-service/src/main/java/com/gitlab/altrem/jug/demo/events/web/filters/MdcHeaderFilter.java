package com.gitlab.altrem.jug.demo.events.web.filters;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.MDC;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import reactor.core.publisher.Mono;
import reactor.util.context.Context;
import reactor.util.context.ContextView;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 10)
public class MdcHeaderFilter
    implements WebFilter {

    private static final String X_CORRELATION_ID = "X-Correlation-Id";

    @Override
    public Mono<Void> filter(
        final ServerWebExchange exchange,
        final WebFilterChain chain) {

        MDC.clear();
        exchange.getResponse().beforeCommit(() -> dumpMdcIntoResponseHeaders(exchange.getResponse()));
        return chain.filter(exchange).contextWrite(addHeadersToContext(exchange));
    }

    private ContextView addHeadersToContext(
        final ServerWebExchange exchange) {

        final Map<String, List<String>> httpHeaders =
            exchange.getRequest().getHeaders().entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        httpHeaders.forEach(this::addToMdc);

        if (!httpHeaders.containsKey(X_CORRELATION_ID)) {
            final String correlationId = UUID.randomUUID().toString();
            MDC.put(X_CORRELATION_ID, correlationId);
            exchange.getRequest().mutate().header(X_CORRELATION_ID, correlationId);
        }

        return Context.of(MDC.getCopyOfContextMap()).readOnly();
    }

    private void addToMdc(
        final String headerKey,
        final List<String> headerValues) {

        MDC.put(headerKey, String.join(",", headerValues));
    }

    private Mono<Void> dumpMdcIntoResponseHeaders(
        final ServerHttpResponse response) {

        return Mono.deferContextual(Mono::just).doOnNext(contextView -> {
            final HttpHeaders headers = response.getHeaders();
            MDC.getCopyOfContextMap().forEach((
                headerName,
                headerValue) -> headers.put(headerName, List.of(headerValue)));
            MDC.clear();
        }).then();
    }
}
