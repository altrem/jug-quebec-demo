package com.gitlab.altrem.jug.demo.events.persistence;

public class EventArtistLink {

    private Integer eventId;

    private String eventName;

    private String eventOrganizationCode;

    private Integer artistId;

    private String artistCode;

    private String artistName;

    public Integer getEventId() {

        return eventId;
    }

    public void setEventId(
        final Integer eventId) {

        this.eventId = eventId;
    }

    public String getEventName() {

        return eventName;
    }

    public void setEventName(
        final String eventName) {

        this.eventName = eventName;
    }

    public String getEventOrganizationCode() {

        return eventOrganizationCode;
    }

    public void setEventOrganizationCode(
        final String eventOrganizationCode) {

        this.eventOrganizationCode = eventOrganizationCode;
    }

    public Integer getArtistId() {

        return artistId;
    }

    public void setArtistId(
        final Integer artistId) {

        this.artistId = artistId;
    }

    public String getArtistCode() {

        return artistCode;
    }

    public void setArtistCode(
        final String artistCode) {

        this.artistCode = artistCode;
    }

    public String getArtistName() {

        return artistName;
    }

    public void setArtistName(
        final String artistName) {

        this.artistName = artistName;
    }
}
