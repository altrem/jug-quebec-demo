package com.gitlab.altrem.jug.demo.events.messaging;

public class JsonEventSummary {

    private Integer id;

    private String name;

    private String organizationCode;

    public Integer getId() {

        return id;
    }

    public void setId(
        final Integer id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(
        final String name) {

        this.name = name;
    }

    public String getOrganizationCode() {

        return organizationCode;
    }

    public void setOrganizationCode(
        final String organizationCode) {

        this.organizationCode = organizationCode;
    }
}
