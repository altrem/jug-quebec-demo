package com.gitlab.altrem.jug.demo.events.domain;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface EventRepository {

    Flux<Event> findAllByCriteria(
        EventSearchFilters searchFilters);

    Mono<Event> findById(
        EventId eventId);
}
