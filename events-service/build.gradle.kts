plugins {
    java
    id("org.springframework.boot")
}

group = rootProject.group
version = rootProject.version

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot", "spring-boot-starter-actuator")
    implementation("org.springframework.boot", "spring-boot-starter-webflux")
    implementation("org.springframework.boot", "spring-boot-starter-validation")
    implementation("org.springframework.boot", "spring-boot-starter-data-jpa")
    implementation("org.springframework.data", "spring-data-r2dbc")

    implementation("io.micrometer", "micrometer-core")
    implementation("io.micrometer", "micrometer-registry-prometheus")

    implementation("io.r2dbc:r2dbc-pool")
    implementation("io.r2dbc:r2dbc-postgresql")

    runtimeOnly("io.micrometer", "micrometer-registry-prometheus")
}

tasks.create<Exec>("populateDatabase") {
    environment(mapOf("PGPASSWORD" to "postgrespwd"))
    commandLine(
        "psql",
        "-d",
        "postgres",
        "-h",
        "localhost",
        "-p",
        "5432",
        "-U",
        "postgres",
        "-f",
        "$projectDir/src/main/resources/schema.sql"
    )
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

springBoot {
    buildInfo()
}

tasks.getByName<org.springframework.boot.gradle.tasks.bundling.BootBuildImage>("bootBuildImage") {
    environment = mapOf(
        "BPE_APPLICATION_VERSION" to "${project.version}",
        "BPE_APPLICATION_NAME" to "events.service",
    )
    imageName = "altrem/events-service:${project.version}"
}
