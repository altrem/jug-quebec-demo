plugins {
    java
    id("org.springframework.boot")
}

group = rootProject.group
version = rootProject.version

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot", "spring-boot-starter-actuator")
    implementation("org.springframework.boot", "spring-boot-starter")
    implementation("org.springframework.boot", "spring-boot-starter-web")
    implementation("org.springframework.boot", "spring-boot-starter-validation")
    implementation("org.springframework.boot", "spring-boot-starter-data-jpa")

    implementation("io.micrometer", "micrometer-core")
    implementation("io.micrometer", "micrometer-registry-prometheus")

    implementation("org.postgresql", "postgresql", "42.3.3")
    implementation("com.zaxxer", "HikariCP")
    implementation("org.apache.tomcat", "tomcat-jdbc")
    implementation("org.apache.commons", "commons-lang3")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

springBoot {
    buildInfo()
}

tasks.getByName<org.springframework.boot.gradle.tasks.bundling.BootBuildImage>("bootBuildImage") {
    environment = mapOf(
        "BPE_APPLICATION_VERSION" to "${project.version}",
        "BPE_APPLICATION_NAME" to "events-legacy.service",
    )
    imageName = "altrem/events-legacy-service:${project.version}"
}
