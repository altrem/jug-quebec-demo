package com.gitlab.altrem.jug.demo.legacy.events.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.gitlab.altrem.jug.demo.legacy.events.domain.Event;
import com.gitlab.altrem.jug.demo.legacy.events.domain.EventId;
import com.gitlab.altrem.jug.demo.legacy.events.domain.EventRepository;
import com.gitlab.altrem.jug.demo.legacy.events.domain.EventSearchFilters;

@Component
public class SqlEventRepository
    implements EventRepository {

    private static final String SEARCH_ALL =
        "SELECT * FROM EVENTS e LEFT JOIN ORGANIZATIONS o ON e.EVENT_ORGANIZATION_ID = o.ORGANIZATION_ID";

    private static final String SEARCH_BY_ORGANIZATION =
        "SELECT * FROM EVENTS e LEFT JOIN ORGANIZATIONS o ON e.EVENT_ORGANIZATION_ID = o.ORGANIZATION_ID WHERE o.ORGANIZATION_CODE = :organizationCode";

    private static final String FIND_BY_ID =
        "SELECT * FROM EVENTS e "
            + "LEFT JOIN ORGANIZATIONS o ON o.ORGANIZATION_ID = e.EVENT_ORGANIZATION_ID "
            + "LEFT JOIN EVENT_ARTISTS ea ON ea.EVENT_ID = ea.EVENT_ID "
            + "LEFT JOIN ARTISTS a ON ea.ARTIST_ID = a.ARTIST_ID "
            + "WHERE e.EVENT_ID = :eventId";

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private final EventRowMapper eventRowMapper;

    private final EventArtistLinkRowMapper eventArtistLinkRowMapper;

    @Autowired
    public SqlEventRepository(
        final NamedParameterJdbcTemplate jdbcTemplate,
        final EventRowMapper eventRowMapper,
        final EventArtistLinkRowMapper eventArtistLinkRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.eventRowMapper = eventRowMapper;
        this.eventArtistLinkRowMapper = eventArtistLinkRowMapper;
    }

    @Override
    public List<Event> findAllByCriteria(
        final EventSearchFilters searchFilters) {

        return searchFilters.getOrganizationCode()
            .map(orgCode -> jdbcTemplate.query(SEARCH_BY_ORGANIZATION, paramsSourceWithOrg(orgCode), eventRowMapper))
            .orElseGet(() -> jdbcTemplate.query(SEARCH_ALL, new MapSqlParameterSource(), eventRowMapper));
    }

    @Override
    public Optional<Event> findById(
        final EventId eventId) {

        final List<EventArtistLink> eventArtistLinks =
            jdbcTemplate.query(FIND_BY_ID, new MapSqlParameterSource().addValue("eventId", eventId.getValue()),
                eventArtistLinkRowMapper);

        return Optional.ofNullable(eventRowMapper.map(eventArtistLinks));
    }

    private MapSqlParameterSource paramsSourceWithOrg(
        final String orgCode) {

        return new MapSqlParameterSource().addValue("organizationCode", orgCode);
    }
}
