package com.gitlab.altrem.jug.demo.legacy.events.web;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.altrem.jug.demo.legacy.events.domain.EventId;
import com.gitlab.altrem.jug.demo.legacy.events.domain.EventService;
import com.gitlab.altrem.jug.demo.legacy.events.messaging.JsonEvent;
import com.gitlab.altrem.jug.demo.legacy.events.messaging.JsonEventSummary;

@RestController
@RequestMapping("api/v2/events")
public class EventController {

    private final EventService eventService;

    private final JsonEventMapper jsonEventMapper;

    @Autowired
    public EventController(
        final EventService eventService,
        final JsonEventMapper jsonEventMapper) {

        this.eventService = eventService;
        this.jsonEventMapper = jsonEventMapper;
    }

    @GetMapping
    @RequestMapping(produces = "application/json")
    public List<JsonEventSummary> getEvents(
        @RequestParam(required = false) final String organizationCode) {

        final String standardizedOrganizationCode = organizationCode != null ? organizationCode.toUpperCase() : null;
        return eventService.getEvents(standardizedOrganizationCode).stream().map(jsonEventMapper::mapSummary)
            .collect(Collectors.toList());
    }

    @GetMapping
    @RequestMapping(value = "/{eventId}", produces = "application/json")
    public JsonEvent getEvent(
        @Valid @NotNull @Positive @PathVariable final Integer eventId) {

        final EventId eventIdentifier = EventId.from(eventId);
        return jsonEventMapper.map(eventService.getEventById(eventIdentifier));
    }
}
