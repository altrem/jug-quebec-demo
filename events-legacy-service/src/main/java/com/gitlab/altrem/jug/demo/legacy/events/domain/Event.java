package com.gitlab.altrem.jug.demo.legacy.events.domain;

import java.util.List;

import com.gitlab.altrem.jug.demo.legacy.events.domain.artists.Artist;

public class Event {

    private final EventId id;

    private final String name;

    private final String organizationCode;

    private final List<Artist> attendingArtists;

    public Event(
        final EventId id,
        final String name,
        final String organizationCode,
        final List<Artist> attendingArtists) {

        this.id = id;
        this.name = name;
        this.organizationCode = organizationCode;
        this.attendingArtists = attendingArtists;
    }

    public EventId getId() {

        return id;
    }

    public String getName() {

        return name;
    }

    public String getOrganizationCode() {

        return organizationCode;
    }

    public List<Artist> getAttendingArtists() {

        return attendingArtists;
    }
}
