package com.gitlab.altrem.jug.demo.legacy.events.domain.artists;

public class ArtistId {

    private final Integer id;

    private ArtistId(
        final Integer id) {

        this.id = id;
    }

    public static ArtistId from(
        final Integer id) {

        return new ArtistId(id);
    }

    public Integer getValue() {

        return id;
    }
}
