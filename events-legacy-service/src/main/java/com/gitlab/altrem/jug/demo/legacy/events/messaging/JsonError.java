package com.gitlab.altrem.jug.demo.legacy.events.messaging;

public class JsonError {

    private Integer httpStatus;

    private String errorCode;

    private String errorMessage;

    public Integer getHttpStatus() {

        return httpStatus;
    }

    public void setHttpStatus(
        final Integer httpStatus) {

        this.httpStatus = httpStatus;
    }

    public String getErrorCode() {

        return errorCode;
    }

    public void setErrorCode(
        final String errorCode) {

        this.errorCode = errorCode;
    }

    public String getErrorMessage() {

        return errorMessage;
    }

    public void setErrorMessage(
        final String errorMessage) {

        this.errorMessage = errorMessage;
    }
}
