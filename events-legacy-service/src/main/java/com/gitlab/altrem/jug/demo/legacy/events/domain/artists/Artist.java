package com.gitlab.altrem.jug.demo.legacy.events.domain.artists;

public class Artist {

    private final ArtistId id;

    private final String code;

    private final String name;

    public Artist(
        final ArtistId id,
        final String code,
        final String name) {

        this.id = id;
        this.code = code;
        this.name = name;
    }

    public ArtistId getId() {

        return id;
    }

    public String getName() {

        return name;
    }

    public String getCode() {

        return code;
    }
}
