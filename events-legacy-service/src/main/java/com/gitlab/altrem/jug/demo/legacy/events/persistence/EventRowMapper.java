package com.gitlab.altrem.jug.demo.legacy.events.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.gitlab.altrem.jug.demo.legacy.events.domain.Event;
import com.gitlab.altrem.jug.demo.legacy.events.domain.EventId;
import com.gitlab.altrem.jug.demo.legacy.events.domain.artists.Artist;
import com.gitlab.altrem.jug.demo.legacy.events.domain.artists.ArtistId;

@Component
public class EventRowMapper
    implements RowMapper<Event> {

    @Override
    public Event mapRow(
        final ResultSet rs,
        final int rowNum)
        throws SQLException {

        return new Event(
            EventId.from(rs.getInt("EVENT_ID")),
            rs.getString("EVENT_NAME"),
            rs.getString("ORGANIZATION_CODE"),
            List.of());
    }

    public Event map(
        final List<EventArtistLink> links) {

        if (links.isEmpty()) {
            return null;
        }

        final List<Artist> artists =
            links.stream().filter(l -> l.getArtistId() != null)
                .map(l -> new Artist(ArtistId.from(l.getArtistId()), l.getArtistCode(), l.getArtistName()))
                .collect(Collectors.toList());

        final EventArtistLink eventArtistLink = links.get(0);
        return new Event(
            EventId.from(eventArtistLink.getEventId()),
            eventArtistLink.getEventName(),
            eventArtistLink.getEventOrganizationCode(),
            artists);
    }
}
