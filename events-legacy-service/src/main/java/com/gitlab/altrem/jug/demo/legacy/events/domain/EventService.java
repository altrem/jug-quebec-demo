package com.gitlab.altrem.jug.demo.legacy.events.domain;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EventService {

    private final EventRepository eventRepository;

    @Autowired
    public EventService(
        final EventRepository eventRepository) {

        this.eventRepository = eventRepository;
    }

    public List<Event> getEvents(
        final String organizationCode) {

        final EventSearchFilters searchFilters = EventSearchFilters.empty();
        Optional.ofNullable(organizationCode).filter(StringUtils::isNotEmpty)
            .ifPresent(searchFilters::onOrganizationCode);

        return eventRepository.findAllByCriteria(searchFilters);
    }

    public Event getEventById(
        final EventId eventId) {

        return eventRepository.findById(eventId).orElseThrow(() -> new EventNotFoundException(eventId));
    }
}
