package com.gitlab.altrem.jug.demo.legacy.events.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.gitlab.altrem.jug.demo.legacy.events.domain.EventNotFoundException;
import com.gitlab.altrem.jug.demo.legacy.events.messaging.JsonError;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(EventNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public JsonError eventNotFoundHandler(
        final EventNotFoundException ex) {

        final JsonError jsonError = new JsonError();
        jsonError.setErrorCode(ex.getErrorCode());
        jsonError.setErrorMessage(ex.getMessage());
        jsonError.setHttpStatus(404);

        return jsonError;
    }
}
