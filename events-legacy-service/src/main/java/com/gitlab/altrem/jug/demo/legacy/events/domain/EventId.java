package com.gitlab.altrem.jug.demo.legacy.events.domain;

public class EventId {

    private final Integer id;

    private EventId(
        final Integer id) {

        this.id = id;
    }

    public static EventId from(
        final Integer id) {

        return new EventId(id);
    }

    public Integer getValue() {

        return id;
    }
}
