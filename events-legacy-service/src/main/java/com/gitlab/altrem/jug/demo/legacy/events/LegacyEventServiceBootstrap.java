package com.gitlab.altrem.jug.demo.legacy.events;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = { "com.gitlab.altrem.jug.demo.legacy" })
@EnableJpaRepositories
public class LegacyEventServiceBootstrap {

    public static void main(
        final String[] args) {

        SpringApplication.run(LegacyEventServiceBootstrap.class, args);
    }
}
