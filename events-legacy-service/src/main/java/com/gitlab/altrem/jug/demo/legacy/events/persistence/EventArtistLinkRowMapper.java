package com.gitlab.altrem.jug.demo.legacy.events.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public class EventArtistLinkRowMapper
    implements RowMapper<EventArtistLink> {

    @Override
    public EventArtistLink mapRow(
        final ResultSet rs,
        final int rowNum)
        throws SQLException {

        final EventArtistLink eventArtistLink = new EventArtistLink();
        eventArtistLink.setEventId(rs.getInt("EVENT_ID"));
        eventArtistLink.setEventOrganizationCode(rs.getString("ORGANIZATION_CODE"));
        eventArtistLink.setEventName(rs.getString("EVENT_NAME"));
        eventArtistLink.setArtistId(rs.getInt("ARTIST_ID"));
        eventArtistLink.setArtistCode(rs.getString("ARTIST_CODE"));
        eventArtistLink.setArtistName(rs.getString("ARTIST_NAME"));

        return eventArtistLink;
    }
}
