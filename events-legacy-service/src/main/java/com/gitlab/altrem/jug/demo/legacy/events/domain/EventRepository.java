package com.gitlab.altrem.jug.demo.legacy.events.domain;

import java.util.List;
import java.util.Optional;

public interface EventRepository {

    List<Event> findAllByCriteria(
        EventSearchFilters searchFilters);

    Optional<Event> findById(
        EventId eventId);
}
